from django.db import models
from django.core.files.uploadedfile import InMemoryUploadedFile
from io import BytesIO
from PIL import Image as PilImage
import sys

 # Model to store information about images
class Image(models.Model):

    # Fields for image name, type, original image, and preview image
    name = models.CharField(verbose_name="Name", max_length=200, default="")
    type = models.CharField(verbose_name="Type", max_length=10, default="")
    image = models.ImageField(upload_to="images/", blank=True, verbose_name="Image")
    preview = models.ImageField(
        upload_to="images/preview/", blank=True, verbose_name="Preview"
    )

    # Save method to handle image resizing and preview creation
    def save(self, **kwargs):
        # Set the desired size for the preview image
        output_size = (100, 100)

        # Create an in-memory buffer to store the resized image
        output_thumb = BytesIO()

        # Open the original image using the Python Imaging Library (PIL)
        img = PilImage.open(self.image)

        # Extract the name of the image file without the extension
        img_name = self.image.name.split(".")[0]

        # Check if the original image size exceeds the desired preview size
        if img.height > 100 or img.width > 100:

            # Resize the image to the specified output size
            img = img.resize(output_size)

            # Save the resized image to the in-memory buffer in PNG format
            img.save(output_thumb, format="PNG", quality=90)

        # Extract the file extension of the original image and update the 'type' field
        self.type = self.image.path.split(".")[-1]

        # Create an InMemoryUploadedFile for the preview image
        self.preview = InMemoryUploadedFile(
            output_thumb,
            "ImageField",
            f"{img_name}_preview.png",
            "image/png",
            sys.getsizeof(output_thumb),
            None,
        )

        super(Image, self).save()
