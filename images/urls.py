from django.urls import path, include
from images.views import ImageViewSet

urlpatterns = [
    # URL pattern for listing images and creating new images
    path(
        "", ImageViewSet.as_view({"get": "list", "post": "create"}), name="image-list"
    ),

    # URL pattern for retrieving and deleting a specific image by its primary key
    path(
        "<int:pk>/",
        ImageViewSet.as_view({"get": "retrieve", "delete": "destroy"}),
        name="image-detail",
    ),
    # Add other URLs as needed
]
