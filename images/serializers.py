from rest_framework import serializers
from .models import Image
from drf_extra_fields.fields import Base64ImageField

# Serializer for the Image model
class ImageSerializer(serializers.ModelSerializer):

    # Additional fields for handling image data in base64 format and file format
    image_base64 = Base64ImageField(required=False)
    image_file = serializers.ImageField(required=False)

    # Custom validation to ensure either image_base64 or image_file is provided
    def validate(self, data):
        # Check if either image_base64 or image_file is provided
        image = any([data.get("image_base64"), data.get("image_file")])

        # If neither is provided and it's a new instance (not an update), raise a validation error
        if not image and not self.instance:
            raise serializers.ValidationError("image_base64 or image_file must be set")
        return data

    # Custom create method to handle both base64 and file format images
    def create(self, validated_data: dict):
        # Extract image_base64 and image_file from the validated data
        image_base64 = validated_data.pop("image_base64", None)
        image_file = validated_data.pop("image_file", None)

        # Set the 'image' field in the model to either image_base64 or image_file
        validated_data["image"] = image_base64 or image_file

        # Create a new Image instance with the provided data
        return Image.objects.create(
            name=validated_data.get("name"), image=validated_data.get("image")
        )

    # Metadata for the serializer, specifying the model and fields
    class Meta:
        model = Image
        fields = (
            "id",
            "name",
            "type",
            "image",
            "preview",
            "image_base64",
            "image_file",
        )
        # Specify read-only and write-only fields
        read_only_fields = ["type", "preview", "image"]
        write_only_fields = ["image_base64", "image_file"]
